﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EuchreApp.Models
{
    public enum Number
    {
        Nine,
        Ten,
        Jack,
        Queen,
        King,
        Ace
    }
}
