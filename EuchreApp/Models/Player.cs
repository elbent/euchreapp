﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EuchreApp.Models
{
    public class Player
    {
        public List<Card> Hand { get; set; }
        public string Name { get; set; }
        public int TrickCount { get; set; }

        public Player(string name)
        {
            this.Name = name;
            this.Hand = new List<Card>();
            this.TrickCount = 0;
        }

        public void AddCard(Card card)
        {
            this.Hand.Add(card);
        }
    }
}
