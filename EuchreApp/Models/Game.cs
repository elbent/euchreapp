﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EuchreApp.Models
{
    public class Game
    {
        public Deck GameDeck { get; private set; }
        public Suit TrumpSuit { get; private set; }
        public Stack<Card> Round { get; set; }
        public Player HumanPlayer { get; set; }
        public Player Player2 { get; set; }
        public Player Player3 { get; set; }
        public Player Player4 { get; set; }

        public Game()
        {
            this.GameDeck = new Deck();
            this.TrumpSuit = ChooseTrumpSuit();
            this.Round = new Stack<Card>();
        }

        private Suit ChooseTrumpSuit()
        {
            // Choosing trump randomly for now
            Random random = new Random();
            return (Suit)random.Next(0, 3);
        }

        public void Deal(Player player)
        {
            // Deal five cards
            for (int i = 0; i < 5; i++)
            {
                player.AddCard(this.GameDeck.Draw());
            }
        }
    }
}
