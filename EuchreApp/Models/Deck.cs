﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EuchreApp.Models
{
    public class Deck
    {
        public Stack<Card> Cards { get; private set; }

        public Deck()
        {
            Stack<Card> cards = new Stack<Card>();
            foreach (Suit suit in Enum.GetValues(typeof(Suit)))
            {
                foreach (Number number in Enum.GetValues(typeof(Number)))
                {
                    Card card = new Card(suit, number);
                    cards.Push(card);
                }
            }
            this.Cards = cards;
        }

        public Stack<Card> Shuffle()
        {
            List<Card> shuffledDeck = new List<Card>();
            List<Card> toShuffle = this.Cards.ToList();
            Random random = new Random();
            int deckLength = Cards.Count;

            for (int i = 0; i < deckLength; i++)
            {
                int indexOfCardToShuffle = random.Next(0, toShuffle.Count);
                Card cardToShuffle = (Card)toShuffle[indexOfCardToShuffle];
                shuffledDeck.Add(cardToShuffle);
                toShuffle.RemoveAt(indexOfCardToShuffle);
            }

            this.Cards = new Stack<Card>(shuffledDeck);
            return this.Cards;
        }

        public Card Draw()
        {
            return this.Cards.Pop();
        }
    }
}
