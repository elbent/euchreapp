﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EuchreApp.Models
{
    public enum Suit
    {
        Spades,
        Hearts,
        Diamonds,
        Clubs
    }
}
