﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EuchreApp.Models
{
    public class Card
    {
        public Player PlayedBy { get; set; }
        public Suit Suit { get; private set; }
        public Number Number { get; private set; }
        public Color Color
        {
            get
            {
                if (this.Suit == Suit.Diamonds || this.Suit == Suit.Hearts)
                {
                    return Color.Red;
                }
                return Color.Black;
            }
        }

        public Card(Suit suit, Number number)
        {
            this.Suit = suit;
            this.Number = number;
        }
    }
}
