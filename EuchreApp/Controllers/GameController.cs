﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EuchreApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace EuchreApp.Controllers
{
    public class GameController : Controller
    {
        public IActionResult Index()
        {
            Game game = new Game();
            game.GameDeck.Shuffle();
            game.HumanPlayer = new Player("El");
            game.Deal(game.HumanPlayer);
            return View(game);
        }
    }
}