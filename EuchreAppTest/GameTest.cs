﻿using EuchreApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace EuchreAppTest
{
    [TestClass]
    public class GameTest
    {
        [TestMethod]
        public void TestDeal_GivesPlayer5Cards()
        {
            Game game = new Game();
            Player player = new Player("player1");

            game.Deal(player);

            Assert.AreEqual(5, player.Hand.Count, "Player should have five cards after deal");
        }
    }
}
