using EuchreApp.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace EuchreAppTest
{
    [TestClass]
    public class DeckTest
    {
        [TestMethod]
        public void TestDeck_Has24Cards()
        {
            Deck deck = new Deck();

            Assert.AreEqual(24, deck.Cards.Count, "Deck should have 24 cards");
        }

        [TestMethod]
        public void TestShuffle_ReturnsDifferentDeck()
        {
            Deck deck = new Deck();
            Stack<Card> oldDeck = deck.Cards;
            deck.Shuffle();

            CollectionAssert.AreNotEqual(deck.Cards, oldDeck, "Shuffled deck should be different");
        }

        [TestMethod]
        public void TestDraw_TakesOneCardFromDeck()
        {
            Deck deck = new Deck();
            deck.Draw();

            Assert.AreEqual(23, deck.Cards.Count, "Deck should have 23 cards after draw");
        }
    }
}
